package com.example.maps;

import androidx.fragment.app.FragmentActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, View.OnClickListener {

    private GoogleMap mMap;
    private TextView pregunta,tvaciertos,tvfallados;
    private CheckBox resposta1,resposta2,resposta3,resposta4;
    private pregunta pregunta1;
    private double longitud,latitud;
    private Button next;
    LatLng sydney;
    private int id,aciertos,fallados;
    private ArrayList<pregunta> preguntas;

    // Firebase instance variables
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mMessagesDatabaseReference,mMessagesDatabaseReference1;
    private ChildEventListener mChildEventListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mFirebaseDatabase = FirebaseDatabase.getInstance();


        mMessagesDatabaseReference = mFirebaseDatabase.getReference().child("preguntes");
        //mMessagesDatabaseReference.orderByChild("//id").addChildEventListener(mChildEventListener);

        pregunta = findViewById(R.id.tvpregunta);
        resposta1 = findViewById(R.id.cxresposta1);
        resposta2 = findViewById(R.id.cxresposta2);
        resposta3 = findViewById(R.id.cxresposta3);
        resposta4 = findViewById(R.id.cxresposta4);
        tvaciertos = findViewById(R.id.tvaciertos);
        tvfallados = findViewById(R.id.tvfallados);
        resposta1.setOnClickListener(this);
        resposta2.setOnClickListener(this);
        resposta3.setOnClickListener(this);
        resposta4.setOnClickListener(this);
        next = findViewById(R.id.btnnext);
        next.setOnClickListener(this);
        preguntas = new ArrayList<>();
        id = 0;
        attachDatabaseReadListener();


    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(0, 0);

    }
    private void comprobar(){
        if (id < preguntas.size() ) {
            if (resposta1.isChecked() && (resposta1.getText().equals(preguntas.get(id).getResposta()))) {
                aciertos++;
            } else if (resposta2.isChecked() && (resposta2.getText().equals(preguntas.get(id).getResposta()))) {
                aciertos++;
            } else if (resposta3.isChecked() && (resposta3.getText().equals(preguntas.get(id).getResposta()))) {
                aciertos++;
            } else if (resposta4.isChecked() && (resposta4.getText().equals(preguntas.get(id).getResposta()))) {
                aciertos++;
            } else {
                fallados++;
            }
        }
    }

    private void mostar(){

        id++;
        if (id < preguntas.size() ) {
            pregunta.setText(preguntas.get(id).getPregunta());
            resposta1.setText(preguntas.get(id).getResposta1());
            resposta2.setText(preguntas.get(id).getResposta2());
            resposta3.setText(preguntas.get(id).getResposta3());
            resposta4.setText(preguntas.get(id).getResposta4());
            longitud = preguntas.get(id).getLongitud();
            latitud = preguntas.get(id).getLatitud();
            sydney = new LatLng(latitud, longitud);
            mMap.clear();
            mMap.addMarker(new MarkerOptions().position(sydney));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        }else{
            tvfallados.setText("Fallados = "+fallados);
            tvaciertos.setText("Aciertos = "+aciertos);
        }
    }

    private void attachDatabaseReadListener() {
        if (mChildEventListener == null) {
            mChildEventListener = new ChildEventListener() {

                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    pregunta pregunta1 = dataSnapshot.getValue(pregunta.class);


                    preguntas.add(pregunta1);
                    System.out.println(preguntas);


                    pregunta.setText(preguntas.get(0).getPregunta());
                    resposta1.setText(preguntas.get(0).getResposta1());
                    resposta2.setText(preguntas.get(0).getResposta2());
                    resposta3.setText(preguntas.get(0).getResposta3());
                    resposta4.setText(preguntas.get(0).getResposta4());
                    longitud = preguntas.get(0).getLongitud();
                    latitud = preguntas.get(0).getLatitud();
                    sydney =  new LatLng(latitud, longitud);
                    mMap.addMarker(new MarkerOptions().position(sydney));
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

                }

                public void onChildChanged(DataSnapshot dataSnapshot, String s) {}
                public void onChildRemoved(DataSnapshot dataSnapshot) {}
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {}
                public void onCancelled(DatabaseError databaseError) {}
            };
            mMessagesDatabaseReference.addChildEventListener(mChildEventListener);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == resposta1){
            resposta2.setChecked(false);
            resposta3.setChecked(false);
            resposta4.setChecked(false);
        }
        if (v == resposta2){
            resposta1.setChecked(false);
            resposta3.setChecked(false);
            resposta4.setChecked(false);
        }
        if (v == resposta3){
            resposta1.setChecked(false);
            resposta2.setChecked(false);
            resposta4.setChecked(false);
        }
        if (v == resposta4){
            resposta1.setChecked(false);
            resposta3.setChecked(false);
            resposta2.setChecked(false);
        }
        if (v == next){
            comprobar();
            resposta2.setChecked(false);
            resposta3.setChecked(false);
            resposta1.setChecked(false);
            resposta4.setChecked(false);
            mostar();

        }
    }
}

package com.example.maps;

public class pregunta {

    private String pregunta;
    private String resposta1,resposta2,resposta3,resposta4,resposta;
    private int id;
    private double longitud,latitud;

    public pregunta(){}

    public pregunta(int id,String pregunta, String resposta1, String resposta2, String resposta3, String resposta4, double longitud, double latitud,String resposta) {
        this.pregunta = pregunta;
        this.resposta1 = resposta1;
        this.resposta2 = resposta2;
        this.resposta3 = resposta3;
        this.resposta4 = resposta4;
        this.longitud = longitud;
        this.latitud = latitud;
        this.id = id;
        this.resposta = resposta;
    }

    public String getResposta() {
        return resposta;
    }

    public void setResposta(String resposta) {
        this.resposta = resposta;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public String getResposta1() {
        return resposta1;
    }

    public void setResposta1(String resposta1) {
        this.resposta1 = resposta1;
    }

    public String getResposta2() {
        return resposta2;
    }

    public void setResposta2(String resposta2) {
        this.resposta2 = resposta2;
    }

    public String getResposta3() {
        return resposta3;
    }

    public void setResposta3(String resposta3) {
        this.resposta3 = resposta3;
    }

    public String getResposta4() {
        return resposta4;
    }

    public void setResposta4(String resposta4) {
        this.resposta4 = resposta4;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    @Override
    public String toString() {
        return "pregunta{" +
                "pregunta='" + pregunta + '\'' +
                ", resposta1='" + resposta1 + '\'' +
                ", resposta2='" + resposta2 + '\'' +
                ", resposta3='" + resposta3 + '\'' +
                ", resposta4='" + resposta4 + '\'' +
                ", id=" + id +
                ", longitud=" + longitud +
                ", latitud=" + latitud +
                '}';
    }
}
